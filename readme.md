après avoir loadé les fixtures

    - profil Super admin : username : MathMin
                           password : password
						   
    - profil admin : username : JohnDoe
                     password : password2


pour le projet web j'ai rajouté les points suivants :

	- un service Securizer qui vérifie à chaque appel si un utilisateur a les droits pour 
	  accéder à une vue ou une fonctionnalité, de plus il vérifie si il y a des droits hérités 
	
	- un système de sélection de produits par catégorie à partir de la vue principale

	- un repeatedType dans le formulaire registrationType pour valider une deuxième fois le mot de passe

	- le formulaire pour les commentaires apparait dans une modale gérée par un petit script js 

	- un utilisateur ne peut mettre qu'un seul commentaire par produit 
	  sinon le bouton "ajouter un commentaire" est disabled

	- lors de la suppression d'un objet ( produit, commentaire ou user ), une page demandera confirmation avant suppression

	- seul le ROLE_SUPER_ADMIN a le droit de supprimer un utilisateur ( ban ou autre )

	- la page des conditions de vente est un pdf qui est affiché directement dans le navigateur