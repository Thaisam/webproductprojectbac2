<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('captcha', CaptchaType::class, [
            'label' => 'Captcha ',
            'width' => 300,
            'height' => 75,
            'length' => 6,
        ])
        ->add('username', TextType::class, ['label' => 'Nom d\'utilisateur'])
        ->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'options' => ['attr' => ['class' => 'password-field']],
            'required' => true,
            'first_options'  => ['label' => 'Password'],
            'second_options' => ['label' => 'Repeat Password'],
        ])
        ->add('FirstName', TextType::class, ['label' => 'Prénom'])
        ->add('LastName', TextType::class, ['label' => 'Nom'])
        ->add('sexe', ChoiceType::class, [
            'label' => 'Votre sexe',
            'required' => true,
            'choices' => [
                'Homme' => 0,
                'Femme' => 1,
                'Non defini' => 2
            ],
            'expanded' => true
        ] )
        ->add('email', EmailType::class, ['label' => 'Email'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
