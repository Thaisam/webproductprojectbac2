<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom du produit'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description du produit'
            ])
            ->add('created', DateType::class, [
                'label' => 'Date d\'ajout sur le site',
                'data' => new \DateTime(),
                'format' => 'dd MM yyyy',
                ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image du produit',
                'required' => false,
                'attr' => ['placeholder' => 'image']
            ])
            ->add('rupture', ChoiceType::class, [
                'label' => 'Produit en rupture ?',
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1
                ]
            ])
            ->add('promotion', IntegerType::class, [
                'label' => 'promotion sur le produit (%)'
            ])
            ->add('TVA', IntegerType::class, [
                'label' => 'TVA du produit (%)'
            ])
            ->add('prixInitial', NumberType::class, [
                'label' => 'Prix du produit TVA non comprise'
            ])
            ->add('category', EntityType::class, [
                'label' => 'Sélectionnez une catégorie',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:Category',
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
