<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Product;
use App\Form\CommentType;
use App\Form\ProductType;
use App\Service\Securizer;
use Cocur\Slugify\Slugify;
use App\Service\NotationService;
use App\Repository\CommentRepository;
use App\Repository\ProductRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Argument\ServiceLocator;

class ProductController extends AbstractController
{
    /**
     * @Route("/", name="products")
     * @param ProductRepository $repository
     * @param CategoryRepository $repository2
     * @param CommentRepository $repository3
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param Securizer $securizer
     * @param NotationService $note
     * @return Response
     */
    public function products(ProductRepository $repository, CategoryRepository $repository2, CommentRepository $repository3, PaginatorInterface $paginator, Request $request, Securizer $securizer, NotationService $note):Response
    {
        $produits = $repository->findBy(
            [],
            ['promotion' => 'DESC', 'created' => 'DESC']
        );

        $notes = array();

        foreach($produits as $prod){
            $moyenne = $note->moyenne($repository3, $prod);
            $notes[] = $moyenne;
        }

        $totalCom = array();

        foreach($produits as $prod){
            $total = $note->totalCom($repository3, $prod);
            $totalCom[] = $total;
        }

        $nbpage = 9;

        $produits = $paginator->paginate(
            $produits,
            $request->query->getInt('page', 1),
            $nbpage
        );

        $page = (!empty($_GET['page']) ? $_GET['page'] : 1);
        
        $categories = $repository2->findAll(

        );

        $user = $this->getUser();
        
        if( $user != null ){
            if( $securizer->isGranted($user, 'ROLE_ADMIN')){
                return $this->render('product/admin/index.html.twig', ['produits' => $produits, 'categories' => $categories, 'notes' => $notes, 'totalCom' => $totalCom, 'page' => $page, 'nbPage' => $nbpage, 'user' => $user]);
            }else { 
                return $this->render('product/user/index.html.twig', ['produits' => $produits, 'categories' => $categories, 'notes' => $notes, 'totalCom' => $totalCom, 'page' => $page, 'nbPage' => $nbpage, 'user' => $user]);
            }
        }else { 
            return $this->render('product/visitor/index.html.twig', ['produits' => $produits, 'categories' => $categories, 'notes' => $notes, 'totalCom' => $totalCom, 'page' => $page, 'nbPage' => $nbpage]);
        }

    }

    
    /**
     * @Route("/categorie/{slugCat}", name="productsByCat")
     * @param ProductRepository $repository
     * @param CategoryRepository $repository2
     * @param CommentRepository $repository3
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param Securizer $securizer
     * @param NotationService $note
     * @return Response
     */
    
    public function productsByCat(ProductRepository $repository, CategoryRepository $repository2, CommentRepository $repository3, PaginatorInterface $paginator, Request $request, $slugCat, Securizer $securizer, NotationService $note):Response
    {
        $categories = $repository2->findAll(
        );

        $category = $repository2->findOneBy(
            ['slugCat' => $slugCat ]
        );

        $produits = $repository->findBy(
            ['category' => $category],
            ['promotion' => 'DESC', 'created' => 'DESC']
        );

        $notes = array();

        foreach($produits as $prod){
            $moyenne = $note->moyenne($repository3, $prod);
            $notes[] = $moyenne;
        }

        $totalCom = array();

        foreach($produits as $prod){
            $total = $note->totalCom($repository3, $prod);
            $totalCom[] = $total;
        }

        $nbpage = 9;

        $produits = $paginator->paginate(
            $produits,
            $request->query->getInt('page', 1),
            $nbpage
        );

        $page = (!empty($_GET['page']) ? $_GET['page'] : 1);

        $user = $this->getUser();

        if( $user != null ){
            if( $securizer->isGranted($user, 'ROLE_ADMIN')){
                return $this->render('product/admin/index.html.twig', ['produits' => $produits, 'categories' => $categories, 'notes' => $notes, 'totalCom' => $totalCom, 'page' => $page, 'nbPage' => $nbpage, 'user' => $user]);
            }else { 
                return $this->render('product/user/index.html.twig', ['produits' => $produits, 'categories' => $categories, 'notes' => $notes, 'totalCom' => $totalCom, 'page' => $page, 'nbPage' => $nbpage, 'user' => $user]);
            }
        }else { 
            return $this->render('product/visitor/index.html.twig', ['produits' => $produits, 'categories' => $categories, 'notes' => $notes, 'totalCom' => $totalCom, 'page' => $page, 'nbPage' => $nbpage]);
        }

        
    }
    

    /**
     * @Route("/product/{slug}", name="product")
     * @param Product $product
     * @param Securizer $securizer
     * @param CommentRepository $repository
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function product(Product $product, Securizer $securizer, CommentRepository $repository, Request $request, EntityManagerInterface $manager): Response
    {

        $comments = $repository->findBy(
            ['prodcom' => $product],
            ['created' => 'DESC']
        );

        $comment = new Comment;
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $comment->setUsercom($this->getUser());
            $comment->setProdcom($product);
            $now = new \DateTime('now');
            if(empty($comment->getCreated())) $comment->setCreated($now);
            $manager->persist($comment);
            $manager->flush();
            $this->addFlash(
                'success',
                'Le commentaire a été correctement ajouté'
            );

            return $this->redirectToRoute('product', ['slug' => $product->getSlug()]);
        }

        $user = $this->getUser();

        $commentForCurrentUser = $repository->findBy(
            ['prodcom' => $product, 'usercom' => $user],
            ['created' => 'DESC']
        );
        
        if( $user != null ){
            if( $securizer->isGranted($user, 'ROLE_ADMIN')){
                return $this->render('product/admin/detail.html.twig', ['product' => $product, 'comments' => $comments, 'form' => $form->createView(), 'user' => $user, 'comcurruser' => $commentForCurrentUser]);
            }else { 
                return $this->render('product/user/detail.html.twig', ['product' => $product, 'comments' => $comments, 'form' => $form->createView(), 'user' => $user, 'comcurruser' => $commentForCurrentUser]);
            }
        }else{
            return $this->render('product/visitor/detail.html.twig', ['product' => $product, 'comments' => $comments, 'form' => $form->createView()]);
        }
        
    }

    /**
     * @Route("/confirmDelProd/{slug}", name="conf_del_prod")
     *
     * @param Product $product
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function confDelProduct(Product $product): Response {

        $user = $this->getUser();

        return $this->render('product/admin/confirmationsuppr.html.twig', [
            'product' => $product, 'user' => $user
        ]);
            
    }


    /**
     * @Route("/delproduct/{slug}", name="del_prod")
     *
     * @param Product $product
     * @param EntityManagerInterface $manager
     * @param CommentRepository $repository
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function delProduct(Product $product, EntityManagerInterface $manager, CommentRepository $repository): Response {

        $comments = $repository->findBy(
            ['prodcom' => $product]    
        );

        foreach($comments as $comment){
            $manager->remove($comment);
        }
            $manager->remove($product);
            $manager->flush();
            return $this->redirectToRoute('products');
        
    
    }
    

    /**
     * @Route("/newproduct", name="new_prod")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function newProduct(Request $request, EntityManagerInterface $manager): Response
    {
        $product = new Product;
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $product->setUser($this->getUser());
            if(empty($product->getImage())) $product->setImage('visuel_non_disponible1.png');
            $now = new \DateTime('now');
            if(empty($product->getUpdatedAt())) $product->setUpdatedAt($now);
            $slugify = new Slugify();
            $product->setSlug($slugify->slugify($product->getNom()));
            $manager->persist($product);
            $manager->flush();
            $this->addFlash(
                'success',
                'L\'article a été correctement ajouté'
            );
            return $this->redirectToRoute('products');
        }
        $user = $this->getUser();

        return $this->render('product/admin/add.html.twig', [
            'form' => $form->createView(), 'user' => $user
        ]);
    }
    

    /**
     * @Route("/edit/{slug}", name="edit_prod")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Product $product
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function editProduct(Request $request, EntityManagerInterface $manager, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $product->setSlug($slugify->slugify($product->getNom()));
            $manager->flush();
            return $this->redirectToRoute('products');
        }

        $user = $this->getUser();

        return $this->render('product/admin/edit.html.twig', [
            'form' => $form->createView(), 'user' => $user
        ]);
    }
}
