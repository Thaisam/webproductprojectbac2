<?php

namespace App\Controller;

use Knp\Snappy\Pdf;
use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PageController extends AbstractController
{
    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        $user = $this->getUser();
        return $this->render('page/about.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/team", name="team")
     * @param UserRepository $repository
     */
    public function team(UserRepository $repository)
    {

        $usersuperadmin = $repository->findOneBy(
            ['username' => 'MathMin']
        );

        $useradmin = $repository->findOneBy(
            ['username' => 'JohnDoe']
        );


        $user = $this->getUser();
        return $this->render('page/team.html.twig', [
            'user' => $user, 'user1' => $usersuperadmin, 'user2' => $useradmin
        ]);
    }

    /**
     * @Route("/cgv", name="c_g_v")
     */
    public function cGV()
    {

        $user = $this->getUser();
        return $this->render('page/cgv.html.twig', [
            'user' => $user
        ]);
    }
}
