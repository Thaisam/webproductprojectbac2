<?php

namespace App\Controller;


use App\Entity\Comment;
use App\Form\CommentType;
use App\Service\Securizer;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommentController extends AbstractController
{
    /**
     * @Route("/{username}/comment/{phrase}", name="mycomments")
     * @param CommentRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param Securizer $securizer
     * @param String $phrase
     * @IsGranted("ROLE_USER")
     */
    public function mycomments(CommentRepository $repository, PaginatorInterface $paginator, Request $request, Securizer $securizer, String $phrase): Response
    {
        $user = $this->getUser();

        $comments = $repository->findBy(
            ['usercom' => $user],
            ['created' => 'DESC']
        );

        $allcomments = $repository->findBy(
            [],
            ['created' => 'DESC']
        );

        $comments = $paginator->paginate(
            $comments,
            $request->query->getInt('page', 1),
            3
        );

        $allcomments = $paginator->paginate(
            $allcomments,
            $request->query->getInt('page', 1),
            5
        );

        /* variable pour switcher entre mycomments et allcomments quand on est admin */ 
        if($phrase == 'mycomments'){
            $bool = false;
            
        }elseif($phrase == 'allcomments'){
            $bool = true;
        }
        
        if( $user != null ){
            if( $securizer->isGranted($user, 'ROLE_ADMIN')){
                return $this->render('comment/admin/index.html.twig', [
                    'comments' => $comments, 'user' => $user, 'allcomments' => $allcomments, 'bool' => $bool
                ]);
            }else { 
                return $this->render('comment/user/index.html.twig', [
                    'comments' => $comments, 'user' => $user
                ]);
            }
        }else { 
            return $this->render('comment/user/index.html.twig', [
                'comments' => $comments, 'user' => $user
            ]);
        }
    }

    /**
     * @Route("/{username}/confirmDelCom/{id}", name="conf_del_com")
     *
     * @param Comment $comment
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function confDelComment(Comment $comment, Securizer $securizer): Response {

        $user = $this->getUser();

        return $this->render('comment/user/confirmationsuppr.html.twig', [
            'comment' => $comment, 'user' => $user
        ]);
            
    }

    /**
     * @Route("/{username}/delcomment/{id}", name="del_com")
     *
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function delComment(Comment $comment, EntityManagerInterface $manager): Response {
        $manager->remove($comment);
        $manager->flush();

        $user = $this->getUser();
        $phrase = 'mycomments';
        return $this->redirectToRoute('mycomments', ['username' => $user->getUsername(), 'phrase' => $phrase]);
    }

    /**
     * @Route("/{username}/editcomment/{id}", name="edit_com")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Comment $comment
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function editComment(Request $request, EntityManagerInterface $manager, Comment $comment): Response
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('now');
            $comment->setCreated($now);
            $manager->flush();

            $user = $this->getUser();
            $phrase = 'mycomments';
        return $this->redirectToRoute('mycomments', ['username' => $user->getUsername(), 'phrase' => $phrase]);
        }

        $user = $this->getUser();
        
        return $this->render('comment/user/edit.html.twig', [
            'form' => $form->createView(), 'user' => $user
        ]);
    }
}
