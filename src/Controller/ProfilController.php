<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfilType;
use App\Service\Securizer;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfilController extends AbstractController
{
/**
     * @Route("/{username}/profil/{phrase}", name="myprofil")
     * @param UserRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param Securizer $securizer
     * @param String $phrase
     * @IsGranted("ROLE_USER")
     */
    public function myprofil(UserRepository $repository, PaginatorInterface $paginator, Request $request, Securizer $securizer, String $phrase): Response
    {
        $user = $this->getUser();

        $allusers = $repository->findAll();

        $allusers = $paginator->paginate(
            $allusers,
            $request->query->getInt('page', 1),
            10
        );

        /* variable pour switcher entre myprofil et allprofils quand on est admin */ 
        if($phrase == 'myprofil'){
            $bool = false;
            
        }elseif($phrase == 'allprofils'){
            $bool = true;
        }
        
        if( $user != null ){
            if( $securizer->isGranted($user, 'ROLE_ADMIN')){
                return $this->render('profil/admin/index.html.twig', [
                    'user' => $user, 'allusers' => $allusers, 'bool' => $bool
                ]);
            }else { 
                return $this->render('profil/user/index.html.twig', [
                    'user' => $user
                ]);
            }
        }else { 
            return $this->render('profil/user/index.html.twig', [
                'user' => $user
            ]);
        }
    }

    /**
     * @Route("/{username}/confirmDelUser/{id}", name="conf_del_user")
     *
     * @param User $user2
     * @return Response
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function confDelComment(User $user2, Securizer $securizer): Response {

        $user = $this->getUser();

        return $this->render('profil/admin/confirmationsuppr.html.twig', [
            'user2' => $user2, 'user' => $user
        ]);
            
    }

    /**
     * @Route("/{username}/deluser/{id}", name="del_user")
     *
     * @param User $user
     * @param User $user2
     * @param EntityManagerInterface $manager
     * @param CommentRepository $repository
     * @return Response
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function delUser(User $user, User $user2, EntityManagerInterface $manager, CommentRepository $repository): Response {

        $comments = $repository->findBy(
            ['usercom' => $user2]    
        );

        foreach($comments as $comment){
            $manager->remove($comment);
        }
            $manager->remove($user2);
            $manager->flush();
            $user = $this->getUser();
            $phrase = 'allprofils';
            return $this->redirectToRoute('myprofil', ['username' => $user->getUsername(), 'phrase' => $phrase]);
        
    
    }

    /**
     * @Route("/{username}/editprofil", name="edit_profil")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param User $user
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function editProfil(Request $request, EntityManagerInterface $manager, User $user, UserPasswordEncoderInterface $encoder): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->flush();

            $user = $this->getUser();
            $phrase = 'myprofil';
        return $this->redirectToRoute('myprofil', ['username' => $user->getUsername(), 'phrase' => $phrase]);
        }
        
        return $this->render('profil/user/edit.html.twig', [
            'form' => $form->createView(), 'user' => $user
        ]);
    }
}
