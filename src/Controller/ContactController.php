<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Service\ContactService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     * @param Request $request
     * @param ContactService $contactService
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function contact(Request $request, ContactService $contactService): Response
    {
        $user = $this->getUser();
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $contact->setFirstName($user->getFirstName());
            $contact->setLastName($user->getLastName());
            $contact->setEmail($user->getEmail());
            // Appeler le service et sa méthode sendMail
            $contactService->sendMail($contact);
            $this->addFlash(
                'success',
                'Le message a bien été envoyé'
            );
            return $this->redirectToRoute('products');
        }
        return $this->render('contact/contact.html.twig', [
            'form' => $form->createView(), 'user' => $user
        ]);
    }
}
