<?php
namespace App\Service;

use App\Entity\Comment;
use App\Entity\Product;
use App\Repository\CommentRepository;

class NotationService
{

    public function totalCom(CommentRepository $repository, Product $product){
        $comments = $repository->findBy(
            ['prodcom' => $product]
        );

        $nbCom = (int)count($comments);

        return $nbCom;
    }

    public function moyenne(CommentRepository $repository, Product $product){

        $comments = $repository->findBy(
            ['prodcom' => $product]
        );

        $nbCom = (int)count($comments);

        $sum = 0;
        foreach($comments as $comment){
            $note = $comment->getNote();
            $sum = $sum + $note;
        }

        if($nbCom == 0){
            $moyenne = 0;
        }else{
            $moyenne = (float) ($sum/$nbCom);
        }

        $moyenne = round($moyenne);

        return $moyenne;

    }

}