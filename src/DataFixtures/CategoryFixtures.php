<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Cocur\Slugify\Slugify;

class CategoryFixtures extends Fixture
{
    private $categories = ['Figurine', 'Pull', 'Goodies', 'Casquette', 'Insolite', 'livre'];

    public function load(ObjectManager $manager)
    {
        $slugify = new Slugify();
        foreach($this->categories as $category) {
            $cat = new Category();
            $cat->setName($category);
            $cat->setSlugCat($slugify->slugify($cat->getName()));
            $manager->persist($cat);
        }
        $manager->flush();
    }
}
