<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Comment;
use App\Entity\Product;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $products = $manager->getRepository(Product::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();

        for($i = 1; $i <=40; $i++){
            $comment = new Comment();
            $comment->setSummary($faker->words($faker->numberBetween(1, 3), true))
                    ->setContent($faker->paragraph(2, true))
                    ->setCreated($faker->dateTimeBetween('-30 days', 'now'))
                    ->setNote($faker->numberBetween(0, 5))
                    ->setUsercom($users[$faker->numberBetween(0, count($users) -1)])
                    ->setProdcom($products[$faker->numberBetween(0, count($products) -1)]);
                $manager->persist($comment);
        }

        $manager->flush();
    }

        /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return class-string[]
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ProductFixtures::class
        ];
    }
}
