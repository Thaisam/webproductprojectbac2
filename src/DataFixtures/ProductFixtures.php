<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\User;
use Faker;
use Cocur\Slugify\Slugify;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\Integer;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        $categories = $manager->getRepository(Category::class)->findAll();
        $user = $manager->getRepository(User::class)->findOneBy(['username' => 'MathMin']);
        $slugify = new Slugify();

        for($i = 1; $i <=25; $i++){
            $product = new Product();
            $product->setNom($faker->words($faker->numberBetween(3, 5), true))
                    ->setDescription($faker->paragraphs(3, true))
                    ->setCreated($faker->dateTimeBetween('-30 days', 'now'))
                    ->setImage('visuel_non_disponible1.png')
                    ->setRupture($faker->boolean(10))
                    ->setPrixInitial($faker->numberBetween(15, 150))
                    ->setPromotion($this->promote())
                    ->setTVA(21)
                    ->setUpdatedAt($faker->dateTimeBetween('now'))
                    ->setCategory($categories[$faker->numberBetween(0, count($categories) -1)])
                    ->setUser($user)
                    ->setSlug($slugify->slugify($product->getNom()));
                $manager->persist($product);
        }

        $manager->flush();
    }

    public function promote(){
        $faker = Faker\Factory::create();
        $bool = new Boolean();
        $bool = (bool) $faker->boolean(10);
        $num = new Integer();
        if($bool == 0){
            $num = 0;
        }
        else{
            $num = (int) $faker->numberBetween(1, 9) *10;
        }

        return $num;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return class-string[]
     */
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class
        ];
    }
}
