<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for($i = 1; $i <= 30; $i++) {
            $user = new User();
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setUsername($user->getFirstName().$user->getLastName());
            $user->setEmail($user->getFirstName().'.'.$user->getLastName().'@gmail.com');
            $password = $this->encoder->encodePassword($user, 'password');
            $user->setPassword($password);
            $user->setSexe($faker->numberBetween(0, 2));
            $manager->persist($user);
        }
        $user = new User();
        $user->setFirstName('Math');
        $user->setLastName('Min');
        $user->setUsername($user->getFirstName().$user->getLastName());
        $user->setEmail('Math.Min@gmail.com');
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $password = $this->encoder->encodePassword($user, 'password');
        $user->setPassword($password);
        $user->setSexe(2);
        $manager->persist($user);

        $user2 = new User();
        $user2->setFirstName('John');
        $user2->setLastName('Doe');
        $user2->setUsername($user2->getFirstName().$user2->getLastName());
        $user2->setEmail('John.Doe@gmail.com');
        $user2->setRoles(['ROLE_ADMIN']);
        $password = $this->encoder->encodePassword($user2, 'password2');
        $user2->setPassword($password);
        $user2->setSexe(0);
        $manager->persist($user2);

        $manager->flush();
    }
}
