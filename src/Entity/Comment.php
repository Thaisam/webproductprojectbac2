<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $summary;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="smallint")
     * @assert\Range(
     *      min="0", 
     *      max="5", 
     *      minMessage="la note doit être supérieure ou égale à zero", 
     *      maxMessage="la note doit être inférieure ou égale à 5"
     * )
     */
    private $note;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usercom;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $prodcom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getUsercom(): ?User
    {
        return $this->usercom;
    }

    public function setUsercom(?User $usercom): self
    {
        $this->usercom = $usercom;

        return $this;
    }

    public function getProdcom(): ?Product
    {
        return $this->prodcom;
    }

    public function setProdcom(?Product $prodcom): self
    {
        $this->prodcom = $prodcom;

        return $this;
    }
}
